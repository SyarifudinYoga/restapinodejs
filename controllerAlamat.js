'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data alamat
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_alamat', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah alamat
exports.tambah = function(req,res){
    var kode_alamat = req.body.kode_alamat;
    var nama_dusun = req.body.nama_dusun;
    var nama_kelurahan = req.body.nama_kelurahan;
    var kecamatan = req.body.kecamatan;
    var kode_pos = req.body.kode_pos;
    connection.query('INSERT INTO tb_alamat(kode_alamat,nama_dusun,nama_kelurahan,kecamatan,kode_pos) VALUES(?,?,?,?,?)',
        [kode_alamat,nama_dusun,nama_kelurahan,kecamatan,kode_pos],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah alamat
exports.ubah = function(req,res){
    var kode_alamat = req.body.kode_alamat;
    var nama_dusun = req.body.nama_dusun;
    var nama_kelurahan = req.body.nama_kelurahan;
    var kecamatan = req.body.kecamatan;
    var kode_pos = req.body.kode_pos;
    connection.query('UPDATE tb_alamat SET nama_dusun=?,nama_kelurahan=?,kecamatan=?,kode_pos=? WHERE kode_alamat=?',
        [nama_dusun,nama_kelurahan,kecamatan,kode_pos,kode_alamat],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus
exports.hapus = function(req,res){
    var kode_alamat = req.body.kode_alamat;
    connection.query('DELETE FROM tb_alamat WHERE kode_alamat=?', [kode_alamat],
    function(error,rows,fields){
        if(error){
            console.log(error);
        }else{
            response.ok("Data Berhasil Dihapus", res)
        }
    });
};
'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data ayah
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_ayah', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah ayah
exports.tambah = function(req,res){
    var kode_ayah = req.body.kode_ayah;
    var nama_ayah = req.body.nama_ayah;
    var nik_ayah = req.body.nik_ayah;
    var tahun_lahir_ayah = req.body.tahun_lahir_ayah;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;

    connection.query('INSERT INTO tb_ayah (kode_ayah,nama_ayah,nik_ayah,tahun_lahir_ayah,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus) VALUES(?,?,?,?,?,?,?,?)',
        [kode_ayah,nama_ayah,nik_ayah,tahun_lahir_ayah,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah ayah
exports.ubah = function(req,res){
    var kode_ayah = req.body.kode_ayah;
    var nama_ayah = req.body.nama_ayah;
    var nik_ayah = req.body.nik_ayah;
    var tahun_lahir_ayah = req.body.tahun_lahir_ayah;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;

    connection.query('UPDATE tb_ayah SET nama_ayah=?,nik_ayah=?,tahun_lahir_ayah=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=?,kode_kebutuhan_khusus=? WHERE kode_ayah=?',
        [nama_ayah,nik_ayah,tahun_lahir_ayah,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus,kode_ayah],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus ayah
exports.hapus = function(req,res){
    var kode_ayah = req.body.kode_ayah;
    connection.query('DELETE FROM tb_ayah WHERE kode_ayah=?',[kode_ayah],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
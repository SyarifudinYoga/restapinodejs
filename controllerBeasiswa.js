'use strict';

var response = require('./res');
var connection = require('./koneksi');


//menampilkan data beasiswa
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_beasiswa', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah beasiswa
exports.tambah = function(req,res){
    var kode_beasiswa = req.body.kode_beasiswa;
    var jenis_beasiswa = req.body.jenis_beasiswa;
    var keterangan_beasiswa = req.body.keterangan_beasiswa;
    var tahun_mulai = req.body.tahun_mulai;
    var tahun_selesai = req.body.tahun_selesai;
    connection.query('INSERT INTO tb_beasiswa(kode_beasiswa,jenis_beasiswa,keterangan_beasiswa,tahun_mulai,tahun_selesai) VALUES(?,?,?,?,?)',
        [kode_beasiswa,jenis_beasiswa,keterangan_beasiswa,tahun_mulai,tahun_selesai],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};


//ubah beasiswa
exports.ubah = function(req,res){
    var kode_beasiswa = req.body.kode_beasiswa;
    var jenis_beasiswa = req.body.jenis_beasiswa;
    var keterangan_beasiswa = req.body.keterangan_beasiswa;
    var tahun_mulai = req.body.tahun_mulai;
    var tahun_selesai = req.body.tahun_selesai;
    connection.query('UPDATE tb_beasiswa SET jenis_beasiswa=?,keterangan_beasiswa=?,tahun_mulai=?,tahun_selesai=? WHERE kode_beasiswa=?',
        [jenis_beasiswa,keterangan_beasiswa,tahun_mulai,tahun_selesai,kode_beasiswa],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus beasiswa
exports.hapus = function(req,res){
    var kode_beasiswa = req.body.kode_beasiswa;
    connection.query('DELETE FROM tb_beasiswa WHERE kode_beasiswa=?',[kode_beasiswa],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
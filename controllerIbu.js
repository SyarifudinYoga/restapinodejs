'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data ibu
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_ibu', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah ibu
exports.tambah = function(req,res){
    var kode_ibu = req.body.kode_ibu;
    var nama_ibu = req.body.nama_ibu;
    var nik_ibu = req.body.nik_ibu;
    var tahun_lahir_ibu = req.body.tahun_lahir_ibu;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;

    connection.query('INSERT INTO tb_ibu (kode_ibu,nama_ibu,nik_ibu,tahun_lahir_ibu,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus) VALUES(?,?,?,?,?,?,?,?)',
        [kode_ibu,nama_ibu,nik_ibu,tahun_lahir_ibu,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah ibu
exports.ubah = function(req,res){
    var kode_ibu = req.body.kode_ibu;
    var nama_ibu = req.body.nama_ibu;
    var nik_ibu = req.body.nik_ibu;
    var tahun_lahir_ibu = req.body.tahun_lahir_ibu;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;

    connection.query('UPDATE tb_ibu SET nama_ibu=?,nik_ibu=?,tahun_lahir_ibu=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=?,kode_kebutuhan_khusus=? WHERE kode_ibu=?',
        [nama_ibu,nik_ibu,tahun_lahir_ibu,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_kebutuhan_khusus,kode_ibu],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus ibu
exports.hapus = function(req,res){
    var kode_ibu = req.body.kode_ibu;
    connection.query('DELETE FROM tb_ibu WHERE kode_ibu=?',[kode_ibu],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
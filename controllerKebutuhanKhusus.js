'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data kebutuhan khusus
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_kebutuhan_khusus', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};


//tambah kebutuhan khusus
exports.tambah = function(req,res){
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;
    var berkebutuhan_khusus = req.body.berkebutuhan_khusus;
    connection.query('INSERT INTO tb_kebutuhan_khusus (kode_kebutuhan_khusus,berkebutuhan_khusus) VALUES(?,?)',
        [kode_kebutuhan_khusus,berkebutuhan_khusus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah kebutuhan khusus
exports.ubah = function(req,res){
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;
    var berkebutuhan_khusus = req.body.berkebutuhan_khusus;
    connection.query('UPDATE tb_kebutuhan_khusus SET berkebutuhan_khusus=? WHERE kode_kebutuhan_khusus=? ',
        [berkebutuhan_khusus,kode_kebutuhan_khusus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus kebutuhan khusus
exports.hapus = function(req,res){
    var kode_kebutuhan_khusus = req.body.kode_kebutuhan_khusus;
    connection.query('DELETE FROM tb_kebutuhan_khusus WHERE kode_kebutuhan_khusus=? ',[kode_kebutuhan_khusus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
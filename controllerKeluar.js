'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data siswa keluar
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_keluar', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah keluar
exports.tambah = function(req,res){
    var kode_keluar = req.body.kode_keluar;
    var keluar_karena = req.body.keluar_karena;
    var tanggal_keluar = req.body.tanggal_keluar;
    var alasan_keluar = req.body.alasan_keluar;
    connection.query('INSERT INTO tb_keluar(kode_keluar,keluar_karena,tanggal_keluar,alasan_keluar) VALUES(?,?,?,?)',
        [kode_keluar,keluar_karena,tanggal_keluar,alasan_keluar],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah keluar
exports.ubah = function(req,res){
    var kode_keluar = req.body.kode_keluar;
    var keluar_karena = req.body.keluar_karena;
    var tanggal_keluar = req.body.tanggal_keluar;
    var alasan_keluar = req.body.alasan_keluar;
    connection.query('UPDATE tb_keluar SET keluar_karena=?,tanggal_keluar=?,alasan_keluar=? WHERE kode_keluar=?',
        [keluar_karena,tanggal_keluar,alasan_keluar,kode_keluar],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus keluar
exports.hapus = function(req,res){
    var kode_keluar = req.body.kode_keluar;
    connection.query('DELETE FROM tb_keluar WHERE kode_keluar=?',[kode_keluar],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};

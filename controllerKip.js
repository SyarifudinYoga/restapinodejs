'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data kip
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_kip', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah kip
exports.tambah = function(req,res){
    var nomor_kip = req.body.nomor_kip;
    var nama_tertera_kip = req.body.nama_tertera_kip;
    var nomor_kks = req.body.nomor_kks;
    connection.query('INSERT INTO tb_kip(nomor_kip,nama_tertera_kip,nomor_kks) VALUES(?,?,?)',
        [nomor_kip,nama_tertera_kip,nomor_kks],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah kip
exports.ubah = function(req,res){
    var nomor_kip = req.body.nomor_kip;
    var nama_tertera_kip = req.body.nama_tertera_kip;
    var nomor_kks = req.body.nomor_kks;
    connection.query('UPDATE tb_kip SET nama_tertera_kip=?,nomor_kks=? WHERE nomor_kip=?',
        [nama_tertera_kip,nomor_kks,nomor_kip],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus kip
exports.hapus = function(req,res){
    var nomor_kip = req.body.nomor_kip;
    connection.query('DELETE FROM tb_kip WHERE nomor_kip=?', [nomor_kip],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};


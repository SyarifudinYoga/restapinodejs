'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data kps
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_kps', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah kps
exports.tambah = function(req,res){
    var no_kps = req.body.no_kps;
    var layak_pip = req.body.layak_pip;
    var alasan_layak = req.body.alasan_layak;
    var penerima_kps = req.body.penerima_kps;
    connection.query('INSERT INTO tb_kps(no_kps,layak_pip,alasan_layak,penerima_kps) VALUES(?,?,?,?)',
        [no_kps,layak_pip,alasan_layak,penerima_kps],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah kps
exports.ubah = function(req,res){
    var no_kps = req.body.no_kps;
    var layak_pip = req.body.layak_pip;
    var alasan_layak = req.body.alasan_layak;
    var penerima_kps = req.body.penerima_kps;
    connection.query('UPDATE tb_kps SET layak_pip=?,alasan_layak=?,penerima_kps=? WHERE no_kps=?',
        [layak_pip,alasan_layak,penerima_kps,no_kps],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus kps
exports.hapus = function(req,res){
    var no_kps = req.body.no_kps;
    connection.query('DELETE FROM tb_kps WHERE no_kps=?',[no_kps],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
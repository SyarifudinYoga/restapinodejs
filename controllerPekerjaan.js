'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data pekerjaan
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_pekerjaan', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah pekerjaan
exports.tambah = function(req,res){
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var nama_pekerjaan = req.body.nama_pekerjaan;
    connection.query('INSERT INTO tb_pekerjaan (kode_pekerjaan,nama_pekerjaan) VALUES(?,?)',
        [kode_pekerjaan,nama_pekerjaan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah pekerjaan
exports.ubah = function(req,res){
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var nama_pekerjaan = req.body.nama_pekerjaan;
    connection.query('UPDATE tb_pekerjaan SET nama_pekerjaan=? WHERE kode_pekerjaan=?',
        [nama_pekerjaan,kode_pekerjaan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus pekerjaan
exports.hapus = function(req,res){
    var kode_pekerjaan = req.body.kode_pekerjaan;
    connection.query('DELETE FROM tb_pekerjaan WHERE kode_pekerjaan=?',[kode_pekerjaan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
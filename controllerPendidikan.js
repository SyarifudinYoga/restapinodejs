'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data pendidikan
exports.viewPendidikan = function(req,res){
    connection.query('SELECT * FROM tb_pendidikan', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//menampilkan data pendidikan by id
exports.getPendidikanById = function(req,res){
    let id = req.params.id;
    connection.query('SELECT * FROM tb_pendidikan WHERE kode_pendidikan =?',[id], 
    function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah pendidikan
exports.addPendidikan = function(req,res){
    var kode_pendidikan = req.body.kode_pendidikan;
    var nama_pendidikan = req.body.nama_pendidikan;
    connection.query('INSERT INTO tb_pendidikan (kode_pendidikan,nama_pendidikan) VALUES(?,?)',
        [kode_pendidikan,nama_pendidikan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah pendidikan
exports.updatePendidikan = function(req,res){
    var kode_pendidikan = req.body.kode_pendidikan;
    var nama_pendidikan = req.body.nama_pendidikan;
    connection.query('UPDATE tb_pendidikan SET nama_pendidikan=? WHERE kode_pendidikan=?',
        [nama_pendidikan,kode_pendidikan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus pendidikan
exports.deletePendidikan = function(req,res){
    var kode_pendidikan = req.body.kode_pendidikan;
    connection.query('DELETE FROM tb_pendidikan WHERE kode_pendidikan=?',[kode_pendidikan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};

//hapus pendidikan ID
exports.deletePendidikanId = function(req,res){
    let id = req.params.id;
    connection.query('DELETE FROM tb_pendidikan WHERE kode_pendidikan=?',[id],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data penghasilan
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tbl_penghasilan', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//menampilkan data penghasilan berdasarkan id
exports.tampilId = function(req,res){
    let id = req.params.id;
    connection.query('SELECT * FROM tbl_penghasilan WHERE kode_penghasilan = ?',[id],
    function(error, rows, fields){
        if(error){
            console.log(error);
        } else {
            response.ok(rows, res)
        }
    });
};

//tambah Penghasilan
exports.tambah = function(req,res){
    var kode_penghasilan = req.body.kode_penghasilan;
    var penghasilan_bulanan = req.body.penghasilan_bulanan;
    connection.query('INSERT INTO tbl_penghasilan (kode_penghasilan,penghasilan_bulanan) VALUES(?,?)',
        [kode_penghasilan,penghasilan_bulanan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah Penghasilan
exports.ubah = function(req,res){
    var kode_penghasilan = req.body.kode_penghasilan;
    var penghasilan_bulanan = req.body.penghasilan_bulanan;
    connection.query('UPDATE tbl_penghasilan SET penghasilan_bulanan=? WHERE kode_penghasilan=? ',
        [penghasilan_bulanan,kode_penghasilan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus Penghasilan
exports.hapus = function(req,res){
    var kode_penghasilan = req.body.kode_penghasilan;
    connection.query('DELETE FROM tbl_penghasilan WHERE kode_penghasilan=? ',[kode_penghasilan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
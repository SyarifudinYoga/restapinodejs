'use strict';

var response = require('./res');
var connection = require('./koneksi');


//menampilkan data prestasi
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_prestasi', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};


//tambah prestasi
exports.tambah = function(req,res){
    var kode_prestasi = req.body.kode_prestasi;
    var jenis_prestasi = req.body.jenis_prestasi;
    var tingkat_prestasi = req.body.tingkat_prestasi;
    var nama_prestasi = req.body.nama_prestasi;
    var tahun_prestasi = req.body.tahun_prestasi;
    var penyelenggara = req.body.penyelenggara;
    connection.query('INSERT INTO tb_prestasi(kode_prestasi,jenis_prestasi,tingkat_prestasi,nama_prestasi,tahun_prestasi,penyelenggara) VALUES(?,?,?,?,?,?)',
        [kode_prestasi,jenis_prestasi,tingkat_prestasi,nama_prestasi,tahun_prestasi,penyelenggara],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah prestasi
exports.ubah = function(req,res){
    var kode_prestasi = req.body.kode_prestasi;
    var jenis_prestasi = req.body.jenis_prestasi;
    var tingkat_prestasi = req.body.tingkat_prestasi;
    var nama_prestasi = req.body.nama_prestasi;
    var tahun_prestasi = req.body.tahun_prestasi;
    var penyelenggara = req.body.penyelenggara;
    connection.query('UPDATE tb_prestasi SET jenis_prestasi=?,tingkat_prestasi=?,nama_prestasi=?,tahun_prestasi=?,penyelenggara=? WHERE kode_prestasi=?',
        [jenis_prestasi,tingkat_prestasi,nama_prestasi,tahun_prestasi,penyelenggara,kode_prestasi],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus prestasi
exports.hapus = function(req,res){
    var kode_prestasi = req.body.kode_prestasi;
    connection.query('DELETE FROM tb_prestasi WHERE kode_prestasi=?',[kode_prestasi],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
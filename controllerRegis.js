'use strict';

var response = require('./res');
var connection = require('./koneksi');


//menampilkan data regis
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_regis', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah regis
exports.tambah = function(req,res){
    var kode_regis = req.body.kode_regis;
    var jenis_pendaftaran = req.body.jenis_pendaftaran;
    var tanggal_masuk_sekolah = req.body.tanggal_masuk_sekolah;
    connection.query('INSERT INTO tb_regis(kode_regis,jenis_pendaftaran,tanggal_masuk_sekolah) VALUES(?,?,?)',
        [kode_regis,jenis_pendaftaran,tanggal_masuk_sekolah],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah regis
exports.ubah = function(req,res){
    var kode_regis = req.body.kode_regis;
    var jenis_pendaftaran = req.body.jenis_pendaftaran;
    var tanggal_masuk_sekolah = req.body.tanggal_masuk_sekolah;
    connection.query('UPDATE tb_regis SET jenis_pendaftaran=?,tanggal_masuk_sekolah=? WHERE kode_regis=?',
        [jenis_pendaftaran,tanggal_masuk_sekolah,kode_regis],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus regis
exports.hapus = function(req,res){
    var kode_regis = req.body.kode_regis;
    connection.query('DELETE FROM tb_regis WHERE kode_regis=?',[kode_regis],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data diri siswa
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_data_diri', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah Siswa
exports.tambah = function(req,res){
    var nik = req.body.nik;
    var nama_lengkap = req.body.nama_lengkap;
    var jenis_kelamin = req.body.jenis_kelamin;
    var nisn = req.body.nisn;
    var no_kitas = req.body.no_kitas;
    var tempat_lahir = req.body.tempat_lahir;
    var tanggal_lahir = req.body.tanggal_lahir;
    var agama = req.body.agama;
    var kewarganegaraan = req.body.kewarganegaraan;
    var nama_negara = req.body.nama_negara;
    var berkebutuhan_khusus = req.body.berkebutuhan_khusus;
    var kode_alamat = req.body.kode_alamat;
    var alamat_jalan = req.body.alamat_jalan;
    var rt = req.body.rt;
    var rw = req.body.rw;
    var tempat_tinggal = req.body.tempat_tinggal;
    var mode_transportasi = req.body.mode_transportasi;
    var no_kps = req.body.no_kps;
    var no_kip = req.body.no_kip;
    var bank = req.body.bank;
    var nomor_regis_akta_lahir = req.body.nomor_regis_akta_lahir;
    var no_rek_bank = req.body.no_rek_bank;
    var nama_rekening = req.body.nama_rekening;
    var kode_ayah = req.body.kode_ayah;
    var kode_ibu = req.body.kode_ibu;
    var kode_wali = req.body.kode_wali;
    var tinggi_badan = req.body.tinggi_badan;
    var berat_badan = req.body.berat_badan;
    var jarak_ke_sekolah = req.body.jarak_ke_sekolah;
    var sebutkan_jarak = req.body.sebutkan_jarak;
    var waktu_tempuh_jam = req.body.waktu_tempuh_jam;
    var waktu_tempuh_menit = req.body.waktu_tempuh_menit;
    var jumlah_saudara_kandung = req.body.jumlah_saudara_kandung;
    var kode_prestasi = req.body.kode_prestasi;
    var kode_beasiswa = req.body.kode_beasiswa;
    var kode_regis = req.body.kode_regis;
    var nomor_peserta_ujian = req.body.nomor_peserta_ujian;
    var kode_keluar = req.body.kode_keluar;
    connection.query('INSERT INTO tb_data_diri(nik,nama_lengkap,jenis_kelamin,nisn,no_kitas,tempat_lahir,tanggal_lahir,agama,kewarganegaraan,nama_negara,berkebutuhan_khusus,kode_alamat,alamat_jalan,rt,rw,tempat_tinggal,mode_transportasi,no_kps,no_kip,bank,nomor_regis_akta_lahir,no_rek_bank,nama_rekening,kode_ayah,kode_ibu,kode_wali,tinggi_badan,berat_badan,jarak_ke_sekolah,sebutkan_jarak,waktu_tempuh_jam,waktu_tempuh_menit,jumlah_saudara_kandung,kode_prestasi,kode_beasiswa,kode_regis,nomor_peserta_ujian,kode_keluar) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [nik,nama_lengkap,jenis_kelamin,nisn,no_kitas,tempat_lahir,tanggal_lahir,agama,kewarganegaraan,nama_negara,berkebutuhan_khusus,kode_alamat,alamat_jalan,
        rt,rw,tempat_tinggal,mode_transportasi,no_kps,no_kip,bank,nomor_regis_akta_lahir,no_rek_bank,nama_rekening,kode_ayah,kode_ibu,kode_wali,tinggi_badan,berat_badan,
        jarak_ke_sekolah,sebutkan_jarak,waktu_tempuh_jam,waktu_tempuh_menit,jumlah_saudara_kandung,kode_prestasi,kode_beasiswa,kode_regis,nomor_peserta_ujian,
        kode_keluar],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah Siswa
exports.ubah = function(req,res){
    var nik = req.body.nik;
    var nama_lengkap = req.body.nama_lengkap;
    var jenis_kelamin = req.body.jenis_kelamin;
    var nisn = req.body.nisn;
    var no_kitas = req.body.no_kitas;
    var tempat_lahir = req.body.tempat_lahir;
    var tanggal_lahir = req.body.tanggal_lahir;
    var agama = req.body.agama;
    var kewarganegaraan = req.body.kewarganegaraan;
    var nama_negara = req.body.nama_negara;
    var berkebutuhan_khusus = req.body.berkebutuhan_khusus;
    var kode_alamat = req.body.kode_alamat;
    var alamat_jalan = req.body.alamat_jalan;
    var rt = req.body.rt;
    var rw = req.body.rw;
    var tempat_tinggal = req.body.tempat_tinggal;
    var mode_transportasi = req.body.mode_transportasi;
    var no_kps = req.body.no_kps;
    var no_kip = req.body.no_kip;
    var bank = req.body.bank;
    var nomor_regis_akta_lahir = req.body.nomor_regis_akta_lahir;
    var no_rek_bank = req.body.no_rek_bank;
    var nama_rekening = req.body.nama_rekening;
    var kode_ayah = req.body.kode_ayah;
    var kode_ibu = req.body.kode_ibu;
    var kode_wali = req.body.kode_wali;
    var tinggi_badan = req.body.tinggi_badan;
    var berat_badan = req.body.berat_badan;
    var jarak_ke_sekolah = req.body.jarak_ke_sekolah;
    var sebutkan_jarak = req.body.sebutkan_jarak;
    var waktu_tempuh_jam = req.body.waktu_tempuh_jam;
    var waktu_tempuh_menit = req.body.waktu_tempuh_menit;
    var jumlah_saudara_kandung = req.body.jumlah_saudara_kandung;
    var kode_prestasi = req.body.kode_prestasi;
    var kode_beasiswa = req.body.kode_beasiswa;
    var kode_regis = req.body.kode_regis;
    var nomor_peserta_ujian = req.body.nomor_peserta_ujian;
    var kode_keluar = req.body.kode_keluar;
    connection.query('UPDATE tb_data_diri SET nama_lengkap=?, jenis_kelamin=?, nisn=?, no_kitas=?, tempat_lahir=?, tanggal_lahir=?, agama=?, kewarganegaraan=?, nama_negara=?, berkebutuhan_khusus=?, kode_alamat=?, alamat_jalan=?, rt=?, rw=?, tempat_tinggal=?, mode_transportasi=?, no_kps=?, no_kip=?, bank=?, nomor_regis_akta_lahir=?, no_rek_bank=?, nama_rekening=?, kode_ayah=?, kode_ibu=?, kode_wali=?, tinggi_badan=?, berat_badan=?, jarak_ke_sekolah=?, sebutkan_jarak=?, waktu_tempuh_jam=?, waktu_tempuh_menit=?, jumlah_saudara_kandung=?, kode_prestasi=?, kode_beasiswa=?, kode_regis=?, nomor_peserta_ujian=?, kode_keluar=? WHERE nik=?',
        [nama_lengkap,jenis_kelamin,nisn,no_kitas,tempat_lahir,tanggal_lahir,agama,kewarganegaraan,nama_negara,berkebutuhan_khusus,kode_alamat,alamat_jalan,
        rt,rw,tempat_tinggal,mode_transportasi,no_kps,no_kip,bank,nomor_regis_akta_lahir,no_rek_bank,nama_rekening,kode_ayah,kode_ibu,kode_wali,tinggi_badan,berat_badan,
        jarak_ke_sekolah,sebutkan_jarak,waktu_tempuh_jam,waktu_tempuh_menit,jumlah_saudara_kandung,kode_prestasi,kode_beasiswa,kode_regis,nomor_peserta_ujian,
        kode_keluar,nik],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus
exports.hapus = function(req,res){
    var nik = req.body.nik;
    connection.query('DELETE FROM tb_data_diri WHERE nik=?', [nik],
    function(error,rows,fields){
        if(error){
            console.log(error);
        }else{
            response.ok("Data Berhasil Dihapus", res)
        }
    });
};
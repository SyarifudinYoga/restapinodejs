'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data ujian
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tb_ujian', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah ujian
exports.tambah = function(req,res){
    var nomor_peserta_ujian = req.body.nomor_peserta_ujian;
    var no_seri_ijazah = req.body.no_seri_ijazah;
    var no_seri_skhus = req.body.no_seri_skhus;
    connection.query('INSERT INTO tb_ujian(nomor_peserta_ujian,no_seri_ijazah,no_seri_skhus) VALUES(?,?,?)',
        [nomor_peserta_ujian,no_seri_ijazah,no_seri_skhus],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah ujian
exports.ubah = function(req,res){
    var nomor_peserta_ujian = req.body.nomor_peserta_ujian;
    var no_seri_ijazah = req.body.no_seri_ijazah;
    var no_seri_skhus = req.body.no_seri_skhus;
    connection.query('UPDATE tb_ujian SET no_seri_ijazah=?,no_seri_skhus=? WHERE nomor_peserta_ujian=?',
        [no_seri_ijazah,no_seri_skhus,nomor_peserta_ujian],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus ujian
exports.hapus = function(req,res){
    var nomor_peserta_ujian = req.body.nomor_peserta_ujian;
    connection.query('DELETE FROM tb_ujian WHERE nomor_peserta_ujian=?',[nomor_peserta_ujian],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
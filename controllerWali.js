'use strict';

var response = require('./res');
var connection = require('./koneksi');

//menampilkan data wali
exports.tampil = function(req,res){
    connection.query('SELECT * FROM tbl_wali', function(error, rows, fields){
        if(error){
            console.log(error);
        }else{
            response.ok(rows, res)
        }
    });
};

//tambah wali
exports.tambah = function(req,res){
    var kode_wali = req.body.kode_wali;
    var nama_wali = req.body.nama_wali;
    var nik_wali = req.body.nik_wali;
    var tahun_lahir_wali = req.body.tahun_lahir_wali;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    connection.query('INSERT INTO tbl_wali (kode_wali,nama_wali,nik_wali,tahun_lahir_wali,kode_pendidikan,kode_pekerjaan,kode_penghasilan) VALUES(?,?,?,?,?,?,?)',
        [kode_wali,nama_wali,nik_wali,tahun_lahir_wali,kode_pendidikan,kode_pekerjaan,kode_penghasilan],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Ditambahkan", res)
            }
        });
};

//ubah wali
exports.ubah = function(req,res){
    var kode_wali = req.body.kode_wali;
    var nama_wali = req.body.nama_wali;
    var nik_wali = req.body.nik_wali;
    var tahun_lahir_wali = req.body.tahun_lahir_wali;
    var kode_pendidikan = req.body.kode_pendidikan;
    var kode_pekerjaan = req.body.kode_pekerjaan;
    var kode_penghasilan = req.body.kode_penghasilan;
    connection.query('UPDATE tbl_wali SET nama_wali=?,nik_wali=?,tahun_lahir_wali=?,kode_pendidikan=?,kode_pekerjaan=?,kode_penghasilan=? WHERE kode_wali=?',
        [nama_wali,nik_wali,tahun_lahir_wali,kode_pendidikan,kode_pekerjaan,kode_penghasilan,kode_wali],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Diubah", res)
            }
        });
};

//hapus wali
exports.hapus = function(req,res){
    var kode_wali = req.body.kode_wali;
    connection.query('DELETE FROM tbl_wali WHERE kode_wali=?',[kode_wali],
        function(error,rows,fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Data Berhasil Dihapus", res)
            }
        });
};
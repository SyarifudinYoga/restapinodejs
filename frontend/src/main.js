import Vue from 'vue'
import VueRouter from 'vue-router'
 
import App from './App.vue'
import AddPendidikan from './components/AddPendidikan.vue'
import UpdatePendidikan from './components/UpdatePendidikan.vue'
import Index from './components/ViewPendidikan.vue'
 
Vue.use(VueRouter)
 
Vue.config.productionTip = false
 
const routes = [
  {
    name: 'AddPendidikan',
    path: '/pendidikan/add',
    component: AddPendidikan
  },
  {
    name: 'UpdatePendidikan',
    path: '/pendidikan/edit/:id',
    component: UpdatePendidikan
  },
  {
    name: 'Index',
    path: '/',
    component: Index
  },
];
 
const router = new VueRouter({ mode: 'history', routes: routes })
 
new Vue({
  // init router
  router,
  render: h => h(App),
}).$mount('#app')

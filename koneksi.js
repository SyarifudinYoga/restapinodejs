var mysql = require('mysql');

//buat koneksi database
const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'pesertadidik'
});

conn.connect((err)=>{
    if(err) throw err;
    console.log('Sukses Koneksi');
});

module.exports = conn;
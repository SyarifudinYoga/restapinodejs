'use strict';

module.exports = function(app){
    var jsonSaya = require('./controller');
    var controllerSiswa = require('./controllerSiswa');
    var controllerAlamat = require('./controllerAlamat');
    var controllerKip = require('./controllerKip');
    var controllerKps = require('./controllerKps');
    var controllerAyah = require('./controllerAyah');
    var controllerIbu = require('./controllerIbu');
    var controllerWali = require('./controllerWali');
    var controllerPendidikan = require('./controllerPendidikan');
    var controllerPenghasilan = require('./controllerPenghasilan');
    var controllerKebutuhanKhusus = require('./controllerKebutuhanKhusus');
    var controllerPekerjaan = require('./controllerPekerjaan');
    var controllerPrestasi = require('./controllerPrestasi');
    var controllerBeasiswa = require('./controllerBeasiswa');
    var controllerRegis = require('./controllerRegis');
    var controllerUjian = require('./controllerUjian');
    var controllerKeluar = require('./controllerKeluar');


    //siswa
    app.route('/siswa/tampil').get(controllerSiswa.tampil);
    app.route('/siswa/tambah').post(controllerSiswa.tambah);
    app.route('/siswa/ubah').put(controllerSiswa.ubah);
    app.route('/siswa/hapus').delete(controllerSiswa.hapus);

    //alamat
    app.route('/alamat/tampil').get(controllerAlamat.tampil);
    app.route('/alamat/tambah').post(controllerAlamat.tambah);
    app.route('/alamat/ubah').put(controllerAlamat.ubah);
    app.route('/alamat/hapus').delete(controllerAlamat.hapus);

    //kip
    app.route('/kip/tampil').get(controllerKip.tampil);
    app.route('/kip/tambah').post(controllerKip.tambah);
    app.route('/kip/ubah').put(controllerKip.ubah);
    app.route('/kip/hapus').delete(controllerKip.hapus);

    //kps
    app.route('/kps/tampil').get(controllerKps.tampil);
    app.route('/kps/tambah').post(controllerKps.tambah);
    app.route('/kps/ubah').put(controllerKps.ubah);
    app.route('/kps/hapus').delete(controllerKps.hapus);

    //Ayah
    app.route('/ayah/tampil').get(controllerAyah.tampil);
    app.route('/ayah/tambah').post(controllerAyah.tambah);
    app.route('/ayah/ubah').put(controllerAyah.ubah);
    app.route('/ayah/hapus').delete(controllerAyah.hapus);

    //Ibu
    app.route('/ibu/tampil').get(controllerIbu.tampil);
    app.route('/ibu/tambah').post(controllerIbu.tambah);
    app.route('/ibu/ubah').put(controllerIbu.ubah);
    app.route('/ibu/hapus').delete(controllerIbu.hapus);

    //Wali
    app.route('/wali/tampil').get(controllerWali.tampil);
    app.route('/wali/tambah').post(controllerWali.tambah);
    app.route('/wali/ubah').put(controllerWali.ubah);
    app.route('/wali/hapus').delete(controllerWali.hapus);

    //pendidikan
    app.route('/pendidikan/view').get(controllerPendidikan.viewPendidikan);
    app.route('/pendidikan/edit/:id').get(controllerPendidikan.getPendidikanById);
    app.route('/pendidikan/add').post(controllerPendidikan.addPendidikan);
    app.route('/pendidikan/edit').put(controllerPendidikan.updatePendidikan);
    app.route('/pendidikan/delete').delete(controllerPendidikan.deletePendidikan);
    app.route('/pendidikan/delete/:id').delete(controllerPendidikan.deletePendidikanId);

    //penghasilan
    app.route('/penghasilan/tampil').get(controllerPenghasilan.tampil);
    app.route('/penghasilan/tampilId/:id').get(controllerPenghasilan.tampilId);
    app.route('/penghasilan/tambah').post(controllerPenghasilan.tambah);
    app.route('/penghasilan/ubah').put(controllerPenghasilan.ubah);
    app.route('/penghasilan/hapus').delete(controllerPenghasilan.hapus);

    //Kebutuhan Khusus
    app.route('/kebutuhanKhusus/tampil').get(controllerKebutuhanKhusus.tampil);
    app.route('/kebutuhanKhusus/tambah').post(controllerKebutuhanKhusus.tambah);
    app.route('/kebutuhanKhusus/ubah').put(controllerKebutuhanKhusus.ubah);
    app.route('/kebutuhanKhusus/hapus').delete(controllerKebutuhanKhusus.hapus);

    //Pekerjaan
    app.route('/pekerjaan/tampil').get(controllerPekerjaan.tampil);
    app.route('/pekerjaan/tambah').post(controllerPekerjaan.tambah);
    app.route('/pekerjaan/ubah').put(controllerPekerjaan.ubah);
    app.route('/pekerjaan/hapus').delete(controllerPekerjaan.hapus);

    //Prestasi
    app.route('/prestasi/tampil').get(controllerPrestasi.tampil);
    app.route('/prestasi/tambah').post(controllerPrestasi.tambah);
    app.route('/prestasi/ubah').put(controllerPrestasi.ubah);
    app.route('/prestasi/hapus').delete(controllerPrestasi.hapus);

    //Beasiswa
    app.route('/beasiswa/tampil').get(controllerBeasiswa.tampil);
    app.route('/beasiswa/tambah').post(controllerBeasiswa.tambah);
    app.route('/beasiswa/ubah').put(controllerBeasiswa.ubah);
    app.route('/beasiswa/hapus').delete(controllerBeasiswa.hapus);

    //Regis
    app.route('/regis/tampil').get(controllerRegis.tampil);
    app.route('/regis/tambah').post(controllerRegis.tambah);
    app.route('/regis/ubah').put(controllerRegis.ubah);
    app.route('/regis/hapus').delete(controllerRegis.hapus);

    //Ujian
    app.route('/ujian/tampil').get(controllerUjian.tampil);
    app.route('/ujian/tambah').post(controllerUjian.tambah);
    app.route('/ujian/ubah').put(controllerUjian.ubah);
    app.route('/ujian/hapus').delete(controllerUjian.hapus);

    //Keluar
    app.route('/keluar/tampil').get(controllerKeluar.tampil);
    app.route('/keluar/tambah').post(controllerKeluar.tambah);
    app.route('/keluar/ubah').put(controllerKeluar.ubah);
    app.route('/keluar/hapus').delete(controllerKeluar.hapus);

    //Controller Default
    app.route('/').get(jsonSaya.index);
       
    
}